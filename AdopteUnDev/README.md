# AdopteUnDev

> 404 Dev Adoption Project

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate

/!\ Attention pour le bon fonctionnement du projet merci de créer dans AdeptUnDev/ 
un .env et de coller la ligne suivante:

API_HOST=http://localhost:8000

Sans cela pas d'image
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

